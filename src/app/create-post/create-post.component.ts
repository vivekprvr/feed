import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.styl']
})
export class CreatePostComponent implements OnInit {

  specificContact = false;
  constructor(
    public dialogRef: MatDialogRef<CreatePostComponent>
) {}

ngOnInit() {}

onCancel() {
    this.dialogRef.close();
}

  selectSpecific(): void {
    this.specificContact = !this.specificContact;
  }
}

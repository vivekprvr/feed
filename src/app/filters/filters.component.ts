import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.styl']
})
export class FiltersComponent implements OnInit {
  specificContact = false;
  constructor(
    public dialogRef: MatDialogRef<FiltersComponent>
  ) { }

  onCancel() {
    this.dialogRef.close();
}

  ngOnInit(): void {
  }

  selectSpecific(): void {
    this.specificContact = !this.specificContact;
  }

}
